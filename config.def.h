/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

/* -b  option; if 0, dmenu appears at bottom     */
static int topbar = 1;

/* -c option; centers dmenu on screen */
static int centered = 0;

/* minimum width when centered */
static int min_width = 500;

/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	// "VictorMono Nerd Font Mono:size=14"
	"BitstromWera Nerd Font Mono:size=12.5"
};

/* -p  option; prompt to the left of input field */
static const char *prompt = NULL;

static const char *colors[SchemeLast][2] = {
	/*                        fg         bg       */
	[SchemeNorm]          = { "#EEEEEE", "#2A2E32" },
	[SchemeSel]           = { "#2A2E32", "#81A2BE" },
	[SchemeOut]           = { "#EEEEEE", "#1C1C1C" },
	[SchemeNormHighlight] = { "#B3CFE8", "#2A2E32" },
	[SchemeSelHighlight]  = { "#EEEEEE", "#5E7D97" },
	[SchemeOutHighlight]  = { "#B3CFE8", "#1C1C1C" },
};

static const unsigned int alpha = 92;

static const unsigned int alphas[SchemeLast][2] = {
	[SchemeNorm] = { OPAQUE, alpha },
	[SchemeSel]  = { OPAQUE, alpha },
	[SchemeOut]  = { OPAQUE, alpha },
};

/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines = 10;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static const unsigned int border_width = 2;
